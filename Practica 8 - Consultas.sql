﻿USE reservas; -- Practica8

/* Consultas */
/* Realizar las siguientes consultas y crear una vista para cada una de ellas y para las subconsultas necesarias*/


/* Ejercicio 1 - Número de habitaciones por cada hotel (solo indicar el número del hotel) */
CREATE OR REPLACE VIEW consulta1 AS
  SELECT 
    th.NUM_HOTEL,COUNT(*) numHabitaciones 
    FROM 
      tipo_habitacion th 
    GROUP BY 
      th.NUM_HOTEL;

SELECT 
  * 
  FROM 
    consulta1 c;

/* Ejercicio 2 - Número de habitaciones por cada hotel (indicar el nombre y la dirección del hotel) */
CREATE OR REPLACE VIEW consulta2 AS 
  SELECT 
    nombre,h.DOMICILIO,c.numHabitaciones 
    FROM 
      hotel h 
    JOIN 
      consulta1 c USING(NUM_HOTEL);

SELECT 
  * 
  FROM 
    consulta2 c;

/* Ejercicio 3 - Indicar el nombre del hotel con el mayor numero de habitaciones */
CREATE OR REPLACE VIEW consulta3 AS 
  SELECT 
    nombre
    FROM 
      consulta2 c 
    WHERE 
      c.numHabitaciones=
        (
          SELECT 
            MAX(numHabitaciones) 
            FROM 
              consulta2
        );

SELECT 
  * 
  FROM 
    consulta3 c;

/* Ejercicio 4 - Indicar el nombre del hotel (o hoteles) con el segundo mayor numero de habitaciones (es 12 y no 15)  */

-- consulta4_c1 - EL segundo mayor numero de habitaciones 
CREATE OR REPLACE VIEW consulta4_c1 AS
  SELECT 
    MAX(numHabitaciones) numHabitaciones
    FROM 
      consulta2 
    WHERE 
      numHabitaciones<
        (
          SELECT 
            MAX(numHabitaciones) 
            FROM 
              consulta1 c
        );

SELECT * FROM consulta4_c1 c1;

CREATE OR REPLACE VIEW consulta4 AS
  SELECT 
    nombre  
    FROM 
      consulta2 c 
    JOIN
      consulta4_c1 c1 USING(numHabitaciones);

SELECT * FROM consulta4 c;

/* Ejercicio 5 - Indicar el nombre de los hoteles que todavia no tenemos habitaciones introducidas */

-- consulta5_c1 - Numeros de hoteles que no tienen habitaciones introducidas
CREATE OR REPLACE VIEW consulta5_c1 AS
  SELECT 
    h.NUM_HOTEL
    FROM  
      hotel h         
    LEFT JOIN 
      consulta1 c ON h.NUM_HOTEL=c.NUM_HOTEL
    WHERE c.NUM_HOTEL IS NULL;

SELECT 
  * 
  FROM 
    consulta5_c1;

CREATE OR REPLACE VIEW consulta5 AS
  SELECT 
    DISTINCT h.NOMBRE 
    FROM 
      consulta5_c1 c1 
    JOIN
      hotel h USING(NUM_HOTEL);

SELECT 
  * 
  FROM
    consulta5 c;

/* Ejercicio 6 - Indicar el nombre del hotel del cual no se han reservado nunca habitaciones 
                 (teniendo en cuenta solamente los hoteles de los cuales hemos introducido sus habitaciones). */

-- consulta6_c1 numero de habitacion reservadas
CREATE OR REPLACE VIEW consulta6_c1 AS
  SELECT 
    DISTINCT r.NUM_TIPOHAB 
    FROM 
      reserva r;

-- consulta6_c2 numero de hotal y su numero de habitacion no han sido reservados
CREATE OR REPLACE VIEW consulta6_c2 AS
  SELECT 
    DISTINCT th.NUM_HOTEL 
    FROM 
      tipo_habitacion th
    LEFT JOIN
      consulta6_c1 c1 USING(NUM_TIPOHAB)
    WHERE c1.NUM_TIPOHAB;

-- consulta6
CREATE OR REPLACE VIEW consulta6 AS 
  SELECT 
    h.NOMBRE 
    FROM 
      hotel h
    JOIN consulta6_c2 c2 USING(NUM_HOTEL);

SELECT * FROM consulta6 c;
   

/* Ejercicio 7 - Indicar el mes que mas reservas han comenzado. */

CREATE OR REPLACE VIEW consulta7 AS
  SELECT 
    MONTH(r.FECHA_INI) mesesComienzo, COUNT(*) nReservas
    FROM 
      reserva r
    GROUP BY 
      mesesComienzo;

SELECT * FROM consulta7 c;


/* Ejercicio 8 - Listar el num_hotel y los distintos meses en que se han comenzado reservas */

